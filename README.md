## About This Project

This project is to build an internal API for a fake messenger service.

## Requirements for this project
You can run this app with Redis or Postgres so you should run these service separately.
You should have these requirements:
#### Redis on port 6379

#### Postgres with this credentional
host = "localhost"
port = "5432"
user = "postgres"
password = "pass1234"
dbname = "postgres"

## How to install this project

To do that, at first you should run this command,
#### go mod tidy

To run the application you can use these commands

1) with Redis:
#### go run cmd/main.go --db=redis

2) with Postgres
#### go run cmd/main.go

## The end
I hope you enjoy from this project, please tell me your points about this project.

#### Amir Zandieh
#### Amirzandieh1@gamil.com

#### Thanks